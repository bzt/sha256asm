SHA-256 in Protected Mode Assembly
==================================

This implementation of the SHA-256 hash was specially designed for boot loaders. As such it's written for protected mode,
has very small footprint (970 bytes), only uses general purpose registers, and it's very easy on the stack. It was written
in [fasm](http://flatassembler.net).

I haven't unrolled the sha loop for compact code size, but I've used a lookup table to avoid cubic root calculation. If you're
looking for a really fast SHA implementation (also for long mode, using SSE and AVX registers), check out
[project Nayuki](https://www.nayuki.io/page/fast-sha2-hashes-in-x86-assembly) and also read about the hardware features of
[Intel SHA Extensions](https://software.intel.com/en-us/articles/intel-sha-extensions) (only available if CPUID(EAX=7,ECX=0)
returns EBX bit 29 set, and has strict alignment requirements).

Application Interface
---------------------

Uses the same API as OpenSSL, except for simplicity this implementation uses one implicit context in the bss segment.

```
sha256_init
```

Initializes the context. Clobbers EAX.

```
sha256_update buffer, length
```

Adds a buffer to the hash. The underlying function receives it's arguments in ESI and ECX. Clobbers EAX, EDI.

```
sha256_final checksumbuffer
```

Generates the final hash into the given 32 bytes long buffer. The underlying function receives the address in EDI. Clobbers
EAX, EBX, ECX.

That's all, hope it will be useful!

Contributors
------------

I'd like to say thanks to @CandyMan and @WinLego for testing this code and helping with finding bugs!
